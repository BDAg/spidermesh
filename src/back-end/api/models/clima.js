  const mongoose = require('mongoose');

const climaSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    id: String,
    temperatura: String,
    umidade: String,
    rssi: String
});

module.exports = mongoose.model('dados_meteorologicos', climaSchema);