const express = require('express');
const router = express.Router();

const climaController = require('../../api/controllers/clima');


router.get('/:id', climaController.getAllById);
router.get('/', climaController.getDistinct);

module.exports = router;