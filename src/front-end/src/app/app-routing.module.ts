import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { AppComponent } from "./app.component";
import { GridComponent } from './components/grid/grid.component';
import { SensorsListComponent } from './components/sensors-list/sensors-list.component';
import { SensorTableComponent } from './components/sensor-table/sensor-table.component';


const routes: Routes = [
  { path: 'grid', component: GridComponent },
  { path: 'sensors', component: SensorsListComponent },
  { path: 'sensors/:id', component: SensorTableComponent},
  { path: '', redirectTo: '/grid', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
