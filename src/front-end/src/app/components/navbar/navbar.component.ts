import { Component, OnInit, Output, EventEmitter, OnChanges, Input } from '@angular/core';
import { Router } from '@angular/router';


export interface MenuItem {
  name: string;
  path: string;
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  toggleMenu = true;
  grid = false;
  sensors = false;

  public menuItems: Array<MenuItem>;

  constructor(private router: Router) {

  }

  ngOnInit() {
  }

}
