import { Component, OnInit, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

export interface Sensor {
  _id: Object;
  id: string;
  temperatura: string;
  umidade: string;
  rssi: string;
  created_at: string;
}

@Component({
  selector: 'app-sensor-table',
  templateUrl: './sensor-table.component.html',
  styleUrls: ['./sensor-table.component.scss']
})
export class SensorTableComponent implements OnInit, OnChanges {
  id = ''
  dtOptions: DataTables.Settings = {};
  sensors: Sensor[] = [];
  
  dtTrigger: Subject<any> = new Subject();

  constructor(private route: ActivatedRoute, private http: HttpClient) { 
    this.id = this.route.snapshot.paramMap.get("id")
    
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get('http://localhost:3000/clima/'+this.id).subscribe((res: any[]) => {
      let data = [];
      // console.log(res)
      for(let i=0; i < res.length; i++){
        data.push({_id: i,
                        id: res[i]['id'],
                        temperatura: res[i]['temperatura'],
                        umidade: res[i]['umidade'],
                        rssi: res[i]['rssi'],
                        created_at: res[i]['created_at']})
      }
      this.sensors = data;
        // Calling the DT trigger to manually render the table
      this.dtTrigger.next();
    })
  }

  ngOnChanges() {

  }

}
