import { Component, OnInit, OnChanges } from '@angular/core';
import { EChartOption } from 'echarts';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit, OnChanges {
    data = [{
        name: 'Gateway',
        x: 400,
        y: 100
    }]

    links = []

    chartOption: EChartOption = { }

    mergeOption: EChartOption = { };

    constructor(private httpClient: HttpClient) { 
        this.get_sensors()
    }

    ngOnInit() { 
    }

    ngOnChanges(){
        this.get_sensors();
    }

    async get_sensors(){
        await this.httpClient.get("http://localhost:3000/clima").subscribe((res: any[]) => {
            let x = 0
            if(res.length==1){
                x = 400;
            } else {
                x = 800/(res.length-1);
            }
            for(let i=0; i < res.length; i++){
                this.data.push({name: res[i].id,
                                x: (x*i),
                                y: (res[i].rssi*-5)})
                this.links.push({source: res[i].id,
                                 target: 'Gateway',
                                 label: {
                                    normal: {
                                        show: true,
                                        formatter: res[i].rssi
                                    }
                                }
                                })
                
            }
            this.generate_chart([this.data, this.links]);
        })
        return [this.data, this.links]
    }

    generate_chart(data){
        this.mergeOption = {
            title: {
                text: 'Rede'
            },
            tooltip: {},
            animationDurationUpdate: 1500,
            animationEasingUpdate: 'quinticInOut',
            series : [ 
                {
                    type: 'graph',
                    layout: 'none',
                    symbolSize: 50,
                    roam: true,
                    label: {
                        normal: {
                            show: true
                        }
                    },
                    edgeSymbol: ['circle', 'none'],
                    edgeSymbolSize: [4, 10],
                    edgeLabel: {
                        normal: {
                            textStyle: {
                                fontSize: 20
                            }
                        }
                    },
                    data: data[0],
                    // links: [],
                    links: data[1],
                    lineStyle: {
                        normal: {
                            opacity: 0.9,
                            width: 2,
                            curveness: 0
                        }
                    }
                }
            ]
        };
    }
  
    




}
