import { Component, OnInit } from '@angular/core';
import { Subject, Timestamp } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';

export interface Sensor {
  _id: Object;
  id: string;
  temperatura: string;
  umidade: string;
  rssi: string;
  created_at: string;
}

@Component({
  selector: 'app-sensors-list',
  templateUrl: './sensors-list.component.html',
  styleUrls: ['./sensors-list.component.scss']
})
export class SensorsListComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  sensors: Sensor[] = [];
  
  dtTrigger: Subject<any> = new Subject();

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.http.get('http://localhost:3000/clima').subscribe((res: any[]) => {
      let data = [];
      // console.log(res)
      for(let i=0; i < res.length; i++){
        data.push({_id: i,
                        id: res[i]['id'],
                        temperatura: res[i]['temperatura'],
                        umidade: res[i]['umidade'],
                        rssi: res[i]['rssi'],
                        created_at: res[i]['created_at']})
      }
      this.sensors = data;
        // Calling the DT trigger to manually render the table
      this.dtTrigger.next();
    })
  }

  ngOnChanges(){

  }
}
