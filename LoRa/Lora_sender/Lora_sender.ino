#include <DHTesp.h>

#include <SPI.h>
#include <LoRa.h>
#include "SSD1306.h"
#include<Arduino.h>


/** Initialize DHT sensor */
DHTesp dht;
/** Task handle for the light value read task */
TaskHandle_t tempTaskHandle = NULL;
/** Pin number for DHT11 data pin */
int dhtPin = 13;
String valor = "oi";
String t = "0";
String h = "0";
//OLED pins to ESP32 GPIOs via this connecthin:
//OLED_SDA -- GPIO4
//OLED_SCL -- GPIO15
//OLED_RST -- GPIO16
 
SSD1306  display(0x3c, 4, 15);
 
// WIFI_LoRa_32 ports
// GPIO5  -- SX1278's SCK
// GPIO19 -- SX1278's MISO
// GPIO27 -- SX1278's MOSI
// GPIO18 -- SX1278's CS
// GPIO14 -- SX1278's RESET
// GPIO26 -- SX1278's IRQ(Interrupt Request)
 
#define SS      18
#define RST     14
#define DI0     26
#define BAND    915E6  //915E6 
 
int counter = 0;
 
void setup() {
  Serial.begin(115200);
  pinMode(25,OUTPUT); //Send success, LED will bright 1 second
  pinMode(16,OUTPUT);
  digitalWrite(16, LOW);    // set GPIO16 low to reset OLED
  delay(50); 
  digitalWrite(16, HIGH);

  // Initialize temperature sensor
  dht.setup(dhtPin, DHTesp::DHT22);
   
  while (!Serial); //If just the the basic function, must connect to a computer
  // Initialising the UI will init the display too.
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(5,5,"LoRa Sender");
  display.display();
   
  SPI.begin(5,19,27,18);
  LoRa.setPins(SS,RST,DI0);
  Serial.println("LoRa Sender");
  if (!LoRa.begin(BAND)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
  Serial.println("LoRa Initial OK!");
  display.drawString(5,20,"LoRa Initializing OK!");
  display.display();
  delay(2000);
}
void loop() {
  // Reading temperature and humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (it's a very slow sensor)
  TempAndHumidity lastValues = dht.getTempAndHumidity();
  t = String(lastValues.temperature,0);
  h = String(lastValues.humidity,0);
  valor = String(t + "ºc " + h + "%");
  Serial.print("Sending packet: ");
  Serial.println(valor);
  display.clear();
  display.setFont(ArialMT_Plain_16);
  display.drawString(3, 5, "Sending packet ");
  display.drawString(25, 30, String(valor));
  display.display();
  
  // send packet
  LoRa.beginPacket();
  LoRa.print("1");
  LoRa.print("id:0001, sensor1:[Temperatura,");
  LoRa.print(t);
  LoRa.print("],");
  LoRa.print("sensor2:[Umidade,");
  LoRa.print(h);
  LoRa.print("]");
  LoRa.endPacket();
  digitalWrite(25, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(25, LOW);    // turn the LED off by making the voltage LOW
  delay(1000); 


  
  /*LoRa.beginPacket();
  LoRa.print("0");
  LoRa.print(t);
  LoRa.endPacket();
  digitalWrite(25, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(25, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second

  LoRa.beginPacket();
  LoRa.print("1");
  LoRa.print(h);
  LoRa.endPacket();
  digitalWrite(25, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(25, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second*/
}
